// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();;
		}
		MyFood = AFood::StaticClass();
		TArray<AActor*> FoundFoodArray;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), MyFood, FoundFoodArray);

		float CordX = AFood::RandomCoordFoodX();
		float CordY = AFood::RandomCoordFoodY();
		bool IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		while (IsEmpty == 0)
		{
			CordX = AFood::RandomCoordFoodX();
			CordY = AFood::RandomCoordFoodY();
			IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		}
		//FVector FoodCord = FoundFoodArray[0]->GetActorLocation();
		FVector RandomVector(CordX, CordY, 0);
		FoundFoodArray[0]->SetActorLocation(RandomVector);
	}
}




float AFood::RandomCoordFoodY()
{
	float Min = -2000.0;
	float Max = 2000.0;
	float RandomFloatY = FMath::FRandRange(Min, Max);
	return RandomFloatY;
}

float AFood::RandomCoordFoodX()
{
	float Min = -800.0;
	float Max = 800.0;
	float RandomFloatX = FMath::FRandRange(Min, Max);
	return RandomFloatX;
}

bool AFood::IsCoordEmpty(float CoordX, float CoordY)
{
	const FVector Start(CoordX, CoordY, 0);
	const FVector End(CoordX, CoordY, 0);
	TArray<FHitResult> HitArray;
	TArray<AActor*> ActorsToIgnore;
	const bool Hit = UKismetSystemLibrary::SphereTraceMulti(GetWorld(), Start, End, TraceRadius,
		UEngineTypes::ConvertToTraceType(ECC_Camera),0,ActorsToIgnore, EDrawDebugTrace::ForDuration, HitArray, true);
	if (HitArray.IsEmpty()) {
		return 1;
	}
	else {
		return 0;
	}
}




