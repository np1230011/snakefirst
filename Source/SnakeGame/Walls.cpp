// Fill out your copyright notice in the Description page of Project Settings.


#include "Walls.h"
#include "SnakeBase.h"

// Sets default values
AWalls::AWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Wall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	Wall->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Wall->SetCollisionResponseToAllChannels(ECR_Overlap);
	//RootComponent = Wall;

}

// Called when the game starts or when spawned
void AWalls::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWalls::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->IsEndGame = 0;
		Snake->Destroy();
		
	}
}

