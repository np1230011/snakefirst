// Fill out your copyright notice in the Description page of Project Settings.


#include "BadFood.h"
#include "SnakeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetSystemLibrary.h"



/*void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteElement();;
		}
		MyFood = ABadFood::StaticClass();
		TArray<AActor*> FoundBadFoodArray;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), MyFood, FoundBadFoodArray);

		float CordX = AFood::RandomCoordFoodX();
		float CordY = AFood::RandomCoordFoodY();
		bool IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		while (IsEmpty == 0)
		{
			CordX = AFood::RandomCoordFoodX();
			CordY = AFood::RandomCoordFoodY();
			IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		}
		//FVector FoodCord = FoundFoodArray[0]->GetActorLocation();
		FVector RandomVector(CordX, CordY, 0);
		FoundBadFoodArray[0]->SetActorLocation(RandomVector);
	}
}
*/


void ABadFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->DeleteElement();;
		}
		MyFood = ABadFood::StaticClass();
		TArray<AActor*> FoundBadFoodArray;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), MyFood, FoundBadFoodArray);

		float CordX = AFood::RandomCoordFoodX();
		float CordY = AFood::RandomCoordFoodY();
		bool IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		while (IsEmpty == 0)
		{
			CordX = AFood::RandomCoordFoodX();
			CordY = AFood::RandomCoordFoodY();
			IsEmpty = AFood::IsCoordEmpty(CordX, CordY);
		}
		//FVector FoodCord = FoundFoodArray[0]->GetActorLocation();
		FVector RandomVector(CordX, CordY, 0);
		FoundBadFoodArray[0]->SetActorLocation(RandomVector);
	}
}
